import "./App.css";
import BodyComponent from "./component/BodyComponent";
import FooterComponent from "./component/FooterComponent";
import HeaderComponent from "./component/HeaderComponent";

function App() {
  return (
    <div className="App">
      <HeaderComponent />
      <BodyComponent />
      <FooterComponent />
    </div>
  );
}

export default App;
