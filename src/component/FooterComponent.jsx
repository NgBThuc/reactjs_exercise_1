import React, { Component } from "react";

export default class FooterComponent extends Component {
  render() {
    return (
      <div className="py-4 bg-dark text-white">
        Copyright &copy; Your Website 2022
      </div>
    );
  }
}
