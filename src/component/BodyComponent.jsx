import React, { Component } from "react";
import BannerComponent from "./BannerComponent";
import ItemComponent from "./ItemComponent";

export default class BodyComponent extends Component {
  render() {
    return (
      <div>
        <div className="container px-lg-5">
          <BannerComponent />

          <div className="row">
            <div className="col-12 col-lg-3">
              <ItemComponent />
            </div>
            <div className="col-12 col-lg-3">
              <ItemComponent />
            </div>
            <div className="col-12 col-lg-3">
              <ItemComponent />
            </div>
            <div className="col-12 col-lg-3">
              <ItemComponent />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
