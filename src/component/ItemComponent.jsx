/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";

export default class ItemComponent extends Component {
  render() {
    return (
      <div className="mb-4">
        <div className="card">
          <img
            src="https://picsum.photos/200"
            className="card-img-top img-fluid"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text">
              Lorem ipsum dolor sit amet consectetur, adipisicing elit.
              Inventore eos nisi aspernatur!
            </p>
          </div>
          <div className="py-2 border-top">
            <a href="#" className="btn btn-primary">
              Find Out More!
            </a>
          </div>
        </div>
      </div>
    );
  }
}
