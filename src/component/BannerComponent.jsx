import React, { Component } from "react";

export default class BannerComponent extends Component {
  render() {
    return (
      <div className="my-4 p-4 p-lg-5 bg-light rounded-3 text-start">
        <h1 className="display-1">A warm welcome!</h1>
        <p className="fs-4 text-muted">
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Laboriosam
          tempora vero libero esse perspiciatis, ad itaque odio enim temporibus
          voluptate ullam laudantium soluta quam mollitia expedita magni aliquam
          explicabo sed?
        </p>
        <button className="btn btn-primary btn-lg">Call to action</button>
      </div>
    );
  }
}
